package objetosPersonajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector3;

import java.util.Map;


/***
 * @class clase PersonajeBolaPalo
 * @author Ismael Burgos
 * @date 2019/02/26
 */

public class PersonajeBolaPalo {
    private Sprite sprite;
    private Vector3 posicion; //coordenadas x e y en tiles del personaje

    private Camera camera;
    private TiledMap map;

    public PersonajeBolaPalo(TiledMap m, Camera c){
        sprite=new Sprite(new Texture("paloBola.png"));
        this.map=m;
        this.camera=c;




    }

    public void dibujar(Batch b){
        sprite.draw(b);
    }

    public void ponerEnTile(int tileX,int tileY){
        posicion=new Vector3(tileX,tileY,0);
        //¡Cuidado! necesitamos el vector auxiliar, para que no se modifique posición! ¡project y unproject modifican su argumento!
        Vector3 posicionPixels=new Vector3(posicion.x,posicion.y,posicion.z);
        camera.project(posicionPixels);
        //Gdx.app.log("posicion final personaje", tileX + " , " + tileY + " || " + posicion.x + " , " + posicion.y);
        sprite.setPosition(posicionPixels.x,posicionPixels.y);
    }

    public int getTileX(){
        return (int)posicion.x;
    }

    public int getTileY(){
        return (int)posicion.y;
    }

    public TiledMap getMapa(){
        return this.map;
    }

    public void setPosicion(Vector3 posicion) {
        this.posicion = posicion;
    }

    public Vector3 getPosicion() {
        return posicion;
    }

    public Camera getCamera() {
        return camera;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}
