package inputTeclado;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.math.Vector3;

import objetosPersonajes.PersonajeBolaPalo;

/***
 * @class clase Escuchador de teclado
 * @author Ismael Burgos
 * @date 2019/02/26
 */

public class EscuchadorBolaPalo implements InputProcessor {
    private PersonajeBolaPalo personaje;
    private int maxAltura;
    private int maxAnchura;

    public EscuchadorBolaPalo(PersonajeBolaPalo p){
        this.personaje=p;
        this.maxAltura=((TiledMapTileLayer)personaje.getMapa().getLayers().get(0)).getHeight();
        this.maxAnchura=((TiledMapTileLayer)personaje.getMapa().getLayers().get(0)).getWidth();
    }



    @Override
    public boolean keyDown(int keycode) {
        switch (keycode) {
            case Input.Keys.RIGHT:
                if(personaje.getTileX()!=this.maxAnchura-1) {
                    personaje.ponerEnTile(personaje.getTileX() + 1, personaje.getTileY());
                }else{
                    personaje.ponerEnTile(0, personaje.getTileY());
                }
                break;

            case Input.Keys.LEFT:
            if(personaje.getTileX()!=0) {
                personaje.ponerEnTile(personaje.getTileX() - 1, personaje.getTileY());
            }else{
                personaje.ponerEnTile(this.maxAnchura-1, personaje.getTileY());
            }
            break;



        }


        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
