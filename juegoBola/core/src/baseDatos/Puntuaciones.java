package baseDatos;

import java.sql.Timestamp;

/***
 * @class Clase Puntuaciones del juego
 * @author Ismael Burgos
 * @date 2019/03/17
 */

public class Puntuaciones {

    private int objetosComidos; //Objetos comidos de la bola
    private Timestamp inicioPartida; //Fecha de inicio de la partida, debe estar siempre relleno
    private Timestamp finPartida; //Fecha de final de la partida, si vale null es que es la partida en curso

    public Puntuaciones(int bc,Timestamp ip,Timestamp fp){
        this.objetosComidos=bc;
        this.inicioPartida=ip;
        this.finPartida=fp;
    }

    public void incrementarPuntos(){
        this.objetosComidos++;
    }

    public int getObjetosComidos() {
        return objetosComidos;
    }

    public void setObjetosComidos(int objetosComidos) {
        this.objetosComidos = objetosComidos;
    }

    public Timestamp getInicioPartida() {
        return inicioPartida;
    }

    public void setInicioPartida(Timestamp inicioPartida) {
        this.inicioPartida = inicioPartida;
    }

    public Timestamp getFinPartida() {
        return finPartida;
    }

    public void setFinPartida(Timestamp finPartida) {
        this.finPartida = finPartida;
    }
}
