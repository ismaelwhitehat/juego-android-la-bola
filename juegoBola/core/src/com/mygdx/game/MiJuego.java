package com.mygdx.game;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;

import java.util.ArrayList;

import inputs.InputBola;
import personajes.Bola;
import personajes.Obstaculo;

/**
 * @class Clase principal del juego
 * @author Ismael Burgos
 * @date 2019/02/25
 */
public class MiJuego extends ApplicationAdapter {


	private OrthogonalTiledMapRenderer renderer; //Clase auxiliar para renderizar un mapa.
	private OrthographicCamera camera; //Cámara a través de la que veremos el mundo.
	private static int WIDTH; //Aquí almacenaremos la anchura en tiles
	private static int HEIGHT; //Aquí almacenaremos la altura en tiles
	public static final float unitScale = 1 / 32f; //Nos servirá para establecer que la pantalla se divide en tiles de 32 pixeles;
	private SpriteBatch batch;
	Bola bola1;
	Obstaculo obstaculo1;
	private ShapeRenderer shapeRenderer;
	private ArrayList<Obstaculo> obstaculos;


	@Override
	public void create() {
		float w = Gdx.graphics.getWidth(); //Obtenemos la anchura de nuestra pantalla
		float h = Gdx.graphics.getHeight(); //Obtenemos la atura de nuestra pantalla

		TiledMap map = new TmxMapLoader().load("LaBolav2.tmx"); //Cargamos el tilemap desde assets
		renderer = new OrthogonalTiledMapRenderer(map, unitScale); //Establecemos el renderizado del mapa dividido en Tiles de 32 dp.


		camera = new OrthographicCamera(); //Declaramos la cámara a través de la que veremos el mundo
		camera.zoom = 1f; //Establecemos el zoom de la cámara. 0.1 es más cercano que 1. Jugaremos con esto en clase
		WIDTH = ((TiledMapTileLayer) map.getLayers().get(0)).getWidth(); //Obtenemos desde el mapa el número de tiles de ancho de la 1º Capa
		HEIGHT = ((TiledMapTileLayer) map.getLayers().get(0)).getHeight(); //Obtenemos desde el mapa el número de tiles de alto de la 1º Capa
		Gdx.app.log("Width", Float.toString(WIDTH)); //Sacamos por el log el número de tiles de ancho
		Gdx.app.log("Height", Float.toString(HEIGHT)); //Sacamos por el log el número de tiles de alto
		camera.setToOrtho(false, WIDTH, HEIGHT); //Establecemos la cámara, y le decimos cuanto tiene que ocupar.

		camera.position.x = WIDTH / 2;  //Establecemos la posición x de la cámara en función del número de tiles de la anchura. Jugaremos con esto en clase
		camera.position.y = HEIGHT / 2; //Establecemos la posición x de la cámara en función del número de tiles de la anchura. Jugaremos con esto en clase
		Gdx.app.log("camera x", Float.toString(camera.position.x));
		Gdx.app.log("camera x", Float.toString(camera.position.y));
		camera.update(); //Colocamos la Cámara.


		this.batch = new SpriteBatch();
		this.shapeRenderer = new ShapeRenderer();
		bola1 = new Bola(map, camera, 1, 1);
		obstaculo1 = new Obstaculo(map, camera, 4, 7);
		obstaculos = new ArrayList<Obstaculo>();
		InputMultiplexer im = new InputMultiplexer();
		im.addProcessor(new InputBola(bola1));
		Gdx.input.setInputProcessor(im);


	}

	@Override
	public void render() {
		Gdx.gl.glClearColor(1, 1, 0, 0.5f);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		float actualAcelY = Gdx.input.getAccelerometerY();
		float actualAcelX = Gdx.input.getAccelerometerX();


		if (actualAcelY < -3) {
			Gdx.app.log("Aceleración y:", Float.toString(actualAcelY));
			bola1.ponerEnPixel(bola1.getTileX() - 1, bola1.getTileY());

		}


		if (actualAcelY > 3) {
			Gdx.app.log("Aceleración y:", Float.toString(actualAcelY));
			bola1.ponerEnPixel(bola1.getTileX() + 1, bola1.getTileY() + 0);

		}
		if (actualAcelX < -3) {
			Gdx.app.log("Aceleración x:", Float.toString(actualAcelX));
			bola1.ponerEnPixel(bola1.getTileX() + 0, bola1.getTileY());
		}

		if (actualAcelX > 3) {
			Gdx.app.log("Aceleración x:", Float.toString(actualAcelX));
			bola1.ponerEnPixel(bola1.getTileX() + 0, bola1.getTileY());
		}

		camera.update();
		renderer.setView(camera); //Establecemos la vista del mundo a través de la cámara.
		renderer.render(); //Renderizamos la vista
		batch.begin();
		bola1.dibujar(batch);
		obstaculo1.dibujar(batch);

		for (int i = 0; i < obstaculos.size(); i++) {
			obstaculos.get(i).dibujar(batch);
		}

		batch.end();

		shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
		bola1.dibujarHitBox(shapeRenderer);
		obstaculo1.dibujarHitBox(shapeRenderer);

		for (int i = 0; i < obstaculos.size(); i++) {
			obstaculos.get(i).dibujarHitBox(shapeRenderer);
		}
		shapeRenderer.end();


		gestionColisiones();


	}

	@Override
	public void dispose() {
		renderer.dispose(); //Destruimos el objeto que renderiza un mapa, para no tener filtraciones de memoria
		this.shapeRenderer.dispose();
		this.batch.dispose();

	}


	public void gestionColisiones() {
		if (bola1.colisionaCon(obstaculo1)) {


		}

		for (int i = 0; i < obstaculos.size(); i++) {
			if (bola1.colisionaCon(obstaculos.get(i))) {
				obstaculos.remove(i);

			}
		}

	}
}






