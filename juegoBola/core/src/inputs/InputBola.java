package inputs;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.maps.tiled.TiledMapTileLayer;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;
import com.mygdx.game.MiJuego;

import java.util.ArrayList;

import personajes.Bola;
import personajes.Obstaculo;


/***
 * @class InputBola este es el escuchador de teclado de la bola y el palo
 * @author Ismael Burgos
 * @date 2019/02/28
 */

public class InputBola implements InputProcessor {

    private Bola bola; // objeto bola
    private int maxAltura; // maxima altura
    private int maxAnchura; // maxima anchura


    // Constructor
    public InputBola(Bola b){
        this.bola=b;
        this.maxAltura=((TiledMapTileLayer)bola.getMapa().getLayers().get(0)).getHeight();
       this.maxAnchura=((TiledMapTileLayer)bola.getMapa().getLayers().get(0)).getWidth();
    }

    // Metodos de la implementación InputProcessor
    @Override
    public boolean keyDown(int keycode) {

        switch (keycode){
            case Input.Keys.LEFT:
                if(bola.getTileX()!=0) {
                   bola.ponerEnTile(bola.getTileX() - 1, bola.getTileY());

                }else{
                    bola.ponerEnTile(this.maxAnchura-1, bola.getTileY());
                }
                break;

            case Input.Keys.RIGHT:
                if(bola.getTileX()!=this.maxAnchura-1) {
                    bola.ponerEnTile(bola.getTileX() + 1, bola.getTileY());
                }else{
                    bola.ponerEnTile(0, bola.getTileY());
                }
                break;

        }
        return false;
    }

    @Override
    public boolean keyUp(int keycode) {
        return false;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        bola.ponerEnTile(bola.getTileX(), bola.getTileY() + 90);
       // MoveToAction mta=new MoveToAction();
       // mta.setPosition((int)bola.getPosicion().x,Gdx.graphics.getHeight());
     //  bola.addAction(mta);
        return true;
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        return false;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return false;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        return false;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}
