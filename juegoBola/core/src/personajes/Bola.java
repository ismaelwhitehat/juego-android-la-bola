package personajes;


import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;

/***
 * @class clase PersonajeBolaPalo
 * @author Ismael Burgos
 * @date 2019/02/26
 */

public class Bola extends PersonajeBola {

    public Bola(TiledMap m, Camera c, int casillax, int casillay) {
        super(m, c);
        this.setSprite(new Sprite(new Texture("bola.png")));
        this.ponerEnPixel(casillax,casillay);
    }
}
