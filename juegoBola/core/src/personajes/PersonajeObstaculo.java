package personajes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.scenes.scene2d.Actor;

/***
 * @class clase PersonajeObstaculo, en donde se reflejará los obstaculos del juego
 * @author Ismael Burgos
 * @date 2019/03/07
 */

public class PersonajeObstaculo extends Actor {

    private Sprite sprite; // objeto sprite
    private Vector3 posicion; //coordenadas x e y en tiles del personaje

    private Camera camera; // objeto camara
    private TiledMap map; // objeto tilemap del mapa

    // Constructor

    public PersonajeObstaculo(TiledMap m, Camera c){
        this.map=m;
        this.camera=c;
    }

    // Metodos

    public void dibujar(Batch b){
        sprite.draw(b);
    }

    public void ponerEnTile(int tileX,int tileY){
        posicion=new Vector3(tileX,tileY,0);
        Vector3 posicionPixels=new Vector3(posicion.x,posicion.y,posicion.z);
        camera.project(posicionPixels);
        Gdx.app.log("posicion final personaje", tileX + " , " + tileY + " || " + posicion.x + " , " + posicion.y);
        sprite.setPosition(posicionPixels.x,posicionPixels.y);
    }



    public void dibujarHitBox(ShapeRenderer sr){
        sr.setColor(Color.RED);
        sr.rect(sprite.getBoundingRectangle().x,
                sprite.getBoundingRectangle().y,
                sprite.getBoundingRectangle().width,
                sprite.getBoundingRectangle().height);
    }


    public boolean colisionaCon(PersonajeObstaculo o){
        return Intersector.overlaps(this.getHitbox(),o.getHitbox());
    }



    public Rectangle getHitbox(){
        return this.sprite.getBoundingRectangle();
    }

    public int getTileX(){
        return (int)posicion.x;
    }

    public int getTileY(){
        return (int)posicion.y;
    }

    public TiledMap getMapa(){
        return this.map;
    }

    public void setPosicion(Vector3 posicion) {
        this.posicion = posicion;
    }

    public Vector3 getPosicion() {
        return posicion;
    }

    public Camera getCamera() {
        return camera;
    }

    public Sprite getSprite() {
        return sprite;
    }

    public void setSprite(Sprite sprite) {
        this.sprite = sprite;
    }
}


