package personajes;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.maps.tiled.TiledMap;

/***
 * @class Obstaculo, herencia a PersonajeObstaculo y cargando la textura de objetos.png
 * @author Ismael Burgos
 * @date 2019/03/07
 */

public class Obstaculo extends PersonajeObstaculo {

    public Obstaculo(TiledMap m, Camera c, int casillax, int casillay) {
        super(m, c);
        this.setSprite(new Sprite(new Texture("objeto.png")));
        this.ponerEnTile(casillax,casillay);
    }
}
