package basedatosandroid;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import baseDatos.BaseDeDatos;

/***
 * @class BaseDeDatosAndroidOpenHelper clase de base de datos que extiende de SQLLiteOpenHelper
 * @author Ismael Burgos
 * @date 2019/03/17
 */

public class BaseDeDatosAndroidOpenHelper extends SQLiteOpenHelper {

    // constructor pasado un objeto de contexto por parametro

    public BaseDeDatosAndroidOpenHelper(Context c){
        super(c, BaseDeDatos.getDatabaseName(),null,BaseDeDatos.getDatabaseVersion());
    }

    // Metodo onCreate
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(BaseDeDatos.getDatabaseCreationQuery());
    }

    // Metodo onUpgrade
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(BaseDeDatos.getDatabaseUpdateQuery());
    }
}
