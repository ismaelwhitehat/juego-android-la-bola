package com.mygdx.game;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;


/**
 * @class clase Principal en el cual vamos a interactuar con el botón jugar y salir
 * @author Ismael Burgos
 * @date 2019/03/01
 */

public class actividadPrincipal extends AppCompatActivity {

    Button botonPrincipal; // Objeto boton principal
    Button Servicio; // Objeto boton Servicio

    // Metodo onCreate
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_actividad_principal);
        botonPrincipal=(Button)findViewById(R.id.jugar);
        Servicio=(Button)findViewById(R.id.botonservicio);

    }

    // Metodo botonAccionJugar en el cual vamos a pasar a otra pantalla del juego, en este caso pasará a la pantalla de niveles.

    public void botonAccionJugar(View view) {

        Intent intentJugar=new Intent(this, AndroidLauncher.class);
        startActivity(intentJugar);
    }

    // Evento por el cual lanza un mensaje de alerta preguntando si se quiere salir del juego

    @Override
    public void onBackPressed(){

        AlertDialog.Builder builder=new AlertDialog.Builder(this);

        builder.setMessage("¿Seguro de que quieres salir del juego?")
        .setCancelable(false)
                .setPositiveButton("Si", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                       actividadPrincipal. super.onBackPressed();
                    }
                })

                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });

        AlertDialog alertadialogo=builder.create();
        alertadialogo.show();

    }

   // boton que se encargará de lanzar el servicio


    public void botonAccionServicio(View view) {

        Intent servicio=new Intent(this,Servicio.class);
        startService(servicio);


    }
}
